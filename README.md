# Semantic Versioning Example
---
> Branch adalah percabangan yang bisa dimanfaatkan untuk release versioning
---
## Contoh cara membuat branch

1. Membuat branch baru
```sh
git branch 1.0.0
```
2. Masuk ke branch yang telah dibuat
```sh
git checkout 1.0.0
```
3. Lihat posisi branch saat ini
```sh
git branch
```
---
## Membuat perubahan di dalam branch
1. Masuk ke branch yang telah dibuat
```sh
touch this-is-1.0.0
```
2. Simpan perubahan
```sh
git add .
```
3. Masukkan pesan perubahan
```sh
git commit -m "Initial Commit"
```
4. Push ke branch yang telah dibuat
```sh
git push origin 1.0.0
```
---
## Merge file dari branch ke master
> Kemungkinan besar akan terjadi conflict.
1. Masuk ke branch master
```sh
git checkout master
```
2. Pull update terbaru dari remote ke local
```sh
git pull origin master
```
3. Ambil file terbaru dari branch yang di pilih
```sh
git pull origin 1.0.0
```
